<?php

namespace Oauth\Bundle\FrontendBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('OauthFrontendBundle:Default:index.html.twig', array('name' => $name));
    }
}
