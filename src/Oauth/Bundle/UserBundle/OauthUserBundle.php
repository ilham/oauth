<?php

namespace Oauth\Bundle\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class OauthUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
