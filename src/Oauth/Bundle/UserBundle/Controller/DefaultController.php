<?php

namespace Oauth\Bundle\UserBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return new RedirectResponse($this->generateUrl('fos_user_security_login'));
    }
}
